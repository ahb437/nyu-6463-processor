library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_arith.all;		 
use ieee.std_logic_unsigned.all;
entity Decoder is
    Port ( Opcode : in  STD_LOGIC_VECTOR (5 downto 0);
           function_code : in  STD_LOGIC_VECTOR (5 downto 0);
			  decrypt: in STD_LOGIC;
			  encrypt: in STD_LOGIC;
			  output_rdy : out STD_LOGIC;
			  Jump : out STD_LOGIC;
           Load_Memory_To_Reg : out  STD_LOGIC;
           Write_To_Memory : out  STD_LOGIC;
           Branch : out  STD_LOGIC;
           Signal_to_ALU  : out  STD_LOGIC_VECTOR (3 downto 0);
           Source : out  STD_LOGIC;
           Reg : out  STD_LOGIC;
           Write_To_Register : out  STD_LOGIC;
			  Halt : out STD_LOGIC
         );
end Decoder;


architecture Behavioral of Decoder is

begin
Process(Opcode, function_code,decrypt,encrypt)
	begin
		if Opcode = "000000"			
			then
			Signal_to_ALU  <= function_code(3 downto 0);
			Load_Memory_To_Reg <= '0';
			Write_To_Memory <= '0';
			Branch <= '0';
			Source <= '0';
			Reg <= '1';
			Write_To_Register <= '1';
			Jump <= '0';
			Halt <= '1';
			output_rdy <= '0';
		elsif (Opcode = "001100" ) 
			then
				Signal_to_ALU  <= "0000"; 
				Load_Memory_To_Reg <= '0';
				Write_To_Memory <= '0';
				Branch <= '0';
				Source <= '0'; 
				Reg <= '0';               
				Write_To_Register <= '0'; 
				Jump <= '1';
				Halt <= '1';
				output_rdy <= '0';
		elsif (Opcode = "111111" and decrypt = '0' and encrypt = '0' ) 
			then
				Signal_to_ALU  <= "0000"; 
				Load_Memory_To_Reg <= '0';
				Write_To_Memory <= '0';
				Branch <= '0';
				Source <= '0'; 
				Reg <= '0';               
				Write_To_Register <= '0'; 
				Jump <= '0';
				Halt <= '0';
				output_rdy <= '1';
		elsif ((Opcode = "111111" and decrypt = '1')) 
			then
				Signal_to_ALU  <= "0000"; 
				Load_Memory_To_Reg <= '0';
				Write_To_Memory <= '0';
				Branch <= '0';
				Source <= '0'; 
				Reg <= '0';               
				Write_To_Register <= '0'; 
				Jump <= '0';
				Halt <= '1';
            output_rdy <= '0';		
		elsif ((Opcode = "111111" and encrypt = '1') ) 
			then
				Signal_to_ALU  <= "0000"; 
				Load_Memory_To_Reg <= '0';
				Write_To_Memory <= '0';
				Branch <= '0';
				Source <= '0'; 
				Reg <= '0';               
				Write_To_Register <= '0'; 
				Jump <= '0';
				Halt <= '1';
            output_rdy <= '0';					
		else 
		   case Opcode(3 downto 0) is
            when "0001" => Signal_to_ALU <= "0000";
                           Reg <= '0';				
               				Write_To_Register <= '1';
									output_rdy <= '0';
									Halt <= '1';
			   when "0010" => Signal_to_ALU <= "0001"; 
								   Reg <= '0';
									Write_To_Register <= '1';
									output_rdy <= '0';
									Halt <= '1';
			   when "0011" => Signal_to_ALU <= "0010"; 
									Reg <= '0';
									Write_To_Register <= '1';
									output_rdy <= '0';				Halt <= '1';
				when "0100" => Signal_to_ALU <= "0011"; 
									Reg <= '0';
									Write_To_Register <= '1';
									output_rdy <= '0';				Halt <= '1';
				when "0101" => Signal_to_ALU <= "0111"; 
									Reg <= '0';				Halt <= '1';
									Write_To_Register <= '1';
									output_rdy <= '0';				Halt <= '1';
				when "0110" => Signal_to_ALU <= "1000"; 
									Reg <= '0';				Halt <= '1';
									Write_To_Register <= '1';
									output_rdy <= '0';
				when "1001" => Signal_to_ALU <= "0101"; 
									Reg <= '0';				Halt <= '1';
									Write_To_Register <= '1';
									output_rdy <= '0';
				when "1010" => Signal_to_ALU <= "0001"; 
									Reg <= '0';				Halt <= '1';
									Write_To_Register <= '1';
									output_rdy <= '0';
				when "1011" => Signal_to_ALU <= "0110"; 
									Reg <= '0';
									Write_To_Register <= '1';
									output_rdy <= '0';				Halt <= '1';
				when others => Signal_to_ALU <= "0000"; 
									Reg <= '0';
									Write_To_Register <= '1';
									output_rdy <= '0';				Halt <= '1';
           end case;
			  
			  if (Opcode(4 downto 0) = "0111") 
				then 
				Load_Memory_To_Reg <= '1';
			  else 
				Load_Memory_To_Reg <= '0'; 
			  end if; 
			  
			  if Opcode(4 downto 0) = "1000" 
				then 
				Write_To_Memory <= '1'; 
			  else 
			   Write_To_Memory <= '0'; 
			  end if; 
			  			  
			  if (Opcode(4 downto 0) = "1001" or Opcode(4 downto 0) ="1010" or Opcode(4 downto 0) ="1011")   
				then 
				Branch <= '1'; 
				Source <= '0';
				Write_To_Register <= '0'; 
			  else 
				Branch <= '0';
				Source <= '1';
				Write_To_Register <= '1';
			  end if;  		  
			  

		end if;
	end process;
end Behavioral;

