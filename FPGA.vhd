----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    13:19:48 11/19/2018 
-- Design Name: 
-- Module Name:    FPGA - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;

use IEEE.STD_LOGIC_1164.ALL; 
use IEEE.STD_LOGIC_unsigned.ALL;

entity FPGA is
PORT (clk100mhz: IN STD_LOGIC; 
		Reset: IN STD_LOGIC;
		Encrypt: IN STD_LOGIC;
		Decrypt: IN STD_LOGIC;
		in1: in STD_LOGIC;
      in2: in STD_LOGIC;
		sw : in  STD_LOGIC_VECTOR (7 downto 0);
	   toggle: IN STD_LOGIC;
	   select1: IN STD_LOGIC;
		clock_select: IN STD_LOGIC;
		clock2 : IN STD_LOGIC;
		LED: out STD_LOGIC;
	   SSEG_CA 		: out  STD_LOGIC_VECTOR (7 downto 0);
      SSEG_AN 		: out  STD_LOGIC_VECTOR (7 downto 0)
		);
end FPGA;

architecture Behavioral of FPGA is
component Clock_Divider
port ( clk: in std_logic;
       reset: in std_logic;
       clock_out: out std_logic
		 );
end component;

component Integration is
PORT (
clk: in STD_LOGIC;
clr: in STD_LOGIC;
decrypt: in STD_LOGIC;
encrypt: in STD_LOGIC;
Register_24BD : in  STD_LOGIC_VECTOR (31 downto 0);
PC : out STD_LOGIC_VECTOR(31 downto 0);
Reg1: out STD_LOGIC_VECTOR(31 DOWNTO 0);
output_rdy : out STD_LOGIC;
Register_24i : out  STD_LOGIC_VECTOR (31 downto 0);
Register_25i : out  STD_LOGIC_VECTOR (31 downto 0)
);
end component;




component SevenSeg_Top is
    Port ( 
           clk 			: in  STD_LOGIC;
			  HexVal       : in std_logic_vector(31 downto 0);
           SSEG_CA 		: out  STD_LOGIC_VECTOR (7 downto 0);
           SSEG_AN 		: out  STD_LOGIC_VECTOR (7 downto 0)
			);
end component;

signal Encrypt_temp: STD_LOGIC; 
signal Decrypt_temp: STD_LOGIC; 

signal input_clock_divider: STD_LOGIC; 
signal clock_divider_outt: STD_LOGIC; 
signal reset1: STD_LOGIC;
signal Register_To_Write_Addrtt :   STD_LOGIC_VECTOR (4 downto 0);
signal word_stored1t:  STD_LOGIC_VECTOR(31 DOWNTO 0);
signal PCt :  STD_LOGIC_VECTOR(31 downto 0);
signal Reg1t: STD_LOGIC_VECTOR(31 DOWNTO 0);
signal Imemt: STD_LOGIC_VECTOR(31 DOWNTO 0);
signal HexValtemp : std_logic_vector(31 downto 0);
signal Dout31: STD_LOGIC_VECTOR(31 DOWNTO 0);
signal Dout30 : std_logic_vector(31 downto 0);
signal S_CA : std_logic_vector(7 downto 0);
signal S_AN : std_logic_vector(7 downto 0);
signal clock_divider_out: STD_LOGIC;
signal Register_24it : STD_LOGIC_VECTOR (31 downto 0);
signal Register_25it : STD_LOGIC_VECTOR (31 downto 0);
signal din : STD_LOGIC_VECTOR (31 downto 0);
signal output_ready: STD_LOGIC;
signal d : std_logic_vector(1 downto 0);



begin

LED <= output_ready;
 
Encrypt_Temp <= Encrypt;
Decrypt_Temp <= Decrypt;

d <= in2 & in1;

Int: Integration PORT MAP(clock_divider_outt,reset1,Decrypt_temp,Encrypt_temp,din,PCt,Reg1t,output_ready,Register_24it,Register_25it);
CD: Clock_Divider PORT MAP(input_clock_divider,reset1,clock_divider_out);
Seven_Seg: SevenSeg_Top PORT MAP(input_clock_divider,HexValtemp,S_CA,S_AN);
input_clock_divider <= clk100mhz;
reset1 <= Reset;
SSEG_CA <= S_CA;
SSEG_AN <= S_AN;
process(toggle,select1,clock_divider_outt)
begin
if select1 = '0'
	then
	if (toggle = '1')
	then 
	HexValtemp <= Register_24it;
	else
	HexValtemp <= Register_25it; 
	end if;
else
	if (toggle = '1')
	then 
	HexValtemp <= PCt;
	else
	HexValtemp <= Reg1t; 
	end if;
end if;
end process;


process(clock_select)
begin 
if (clock_select = '0')
then 
clock_divider_outt <= clock2;
else 
clock_divider_outt <= clock_divider_out;
end if;
end process;


process(reset1,sw,in1,in2)
begin 

if (reset1 = '0')
then 
case d is
  when "00" =>   din(7 downto 0) <= sw;
  when "01" =>   din(15 downto 8) <= sw;
  when "10" =>    din(23 downto 16) <= sw;
  when "11" =>    din(31 downto 24) <= sw;
  when others => 
  end case;
else 
end if;
end process;

end Behavioral;

