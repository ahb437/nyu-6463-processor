library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_arith.all;		 
use ieee.std_logic_unsigned.all;
entity Arithmetic_Logic_Unit is 
    Port ( Register_S : in  STD_LOGIC_VECTOR (31 downto 0);
           Register_T : in  STD_LOGIC_VECTOR (31 downto 0);
           ALU_CONTROL : in  STD_LOGIC_VECTOR (3 downto 0);
           Zero_Flag : out  STD_LOGIC;
           Result : out  STD_LOGIC_VECTOR (31 downto 0));
end Arithmetic_Logic_Unit;

architecture Behavioral of Arithmetic_Logic_Unit is
signal Temp_Result : STD_LOGIC_VECTOR (31 downto 0);
FUNCTION LS (
   a : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
	B : IN STD_LOGIC_VECTOR(31 DOWNTO 0)
	          )
RETURN STD_LOGIC_VECTOR IS
VARIABLE o : STD_LOGIC_VECTOR(31 DOWNTO 0);
begin
if B(4 DOWNTO 0) = "00001" 
   THEN
	 o:= a(30 DOWNTO 0) & "0";
ELSIF B(4 DOWNTO 0) = "00010" 
   THEN
	 o:=	a(29 DOWNTO 0) & "00";
ELSIF B(4 DOWNTO 0) = "00011"
   THEN
	 o:= a(28 DOWNTO 0) & "000";
ELSIF B(4 DOWNTO 0) = "00100"
   THEN
	 o:= a(27 DOWNTO 0) & "0000";
ELSIF B(4 DOWNTO 0) = "00101"
   THEN
	 o:= a(26 DOWNTO 0) & "00000"; 
ELSIF B(4 DOWNTO 0) = "00110"
   THEN
	 o:= a(25 DOWNTO 0) & "000000"; 
ELSIF B(4 DOWNTO 0) = "00111"
   THEN
	 o:= a(24 DOWNTO 0) & "0000000";
ELSIF B(4 DOWNTO 0) = "01000"
   THEN
	 o:= a(23 DOWNTO 0) & "00000000";
ELSIF B(4 DOWNTO 0) = "01001"
   THEN
	 o:= a(22 DOWNTO 0) & "000000000";
ELSIF B(4 DOWNTO 0) = "01010" 
   THEN
	 o:= a(21 DOWNTO 0) & "0000000000";
ELSIF B(4 DOWNTO 0) = "01011"
   THEN
	 o:= a(20 DOWNTO 0) & "00000000000";
ELSIF B(4 DOWNTO 0) = "01100"
   THEN
	 o:= a(19 DOWNTO 0) & "000000000000";
ELSIF B(4 DOWNTO 0) = "01101"
   THEN
	 o:= a(18 DOWNTO 0) & "0000000000000";	 
ELSIF B(4 DOWNTO 0) = "01110"
   THEN
	 o:= a(17 DOWNTO 0) & "00000000000000"; 
ELSIF B(4 DOWNTO 0) = "01111"
   THEN
	 o:= a(16 DOWNTO 0) & "000000000000000";
ELSIF B(4 DOWNTO 0) = "10000"
   THEN
	 o:= a(15 DOWNTO 0) & "0000000000000000";
ELSIF B(4 DOWNTO 0) = "10001"
   THEN
	 o:= a(14 DOWNTO 0) & "00000000000000000";
ELSIF B(4 DOWNTO 0) = "10010"
   THEN
	 o:= a(13 DOWNTO 0) & "000000000000000000";
ELSIF B(4 DOWNTO 0) = "10011"
   THEN
	 o:= a(12 DOWNTO 0) & "0000000000000000000";
ELSIF B(4 DOWNTO 0) = "10100"
   THEN
	 o:= a(11 DOWNTO 0) & "00000000000000000000";
ELSIF B(4 DOWNTO 0) = "10101"
   THEN
	 o:= a(10 DOWNTO 0) & "000000000000000000000";
ELSIF B(4 DOWNTO 0) = "10110"
   THEN
	 o:= a(9 DOWNTO 0) &  "0000000000000000000000";
ELSIF B(4 DOWNTO 0) = "10111"
   THEN
	 o:= a(8 DOWNTO 0) & "00000000000000000000000";
ELSIF B(4 DOWNTO 0) = "11000"
   THEN
	 o:= a(7 DOWNTO 0) & "000000000000000000000000";
ELSIF B(4 DOWNTO 0) = "11001"
   THEN
	 o:= a(6 DOWNTO 0) & "0000000000000000000000000";
ELSIF B(4 DOWNTO 0) = "11010"
   THEN
	 o:= a(5 DOWNTO 0) & "00000000000000000000000000";
ELSIF B(4 DOWNTO 0) = "11011"
   THEN
	 o:= a(4 DOWNTO 0) & "000000000000000000000000000";
ELSIF B(4 DOWNTO 0) = "11100"
   THEN
	 o:= a(3 DOWNTO 0) & "0000000000000000000000000000";
ELSIF B(4 DOWNTO 0) = "11101"
   THEN
	 o:= a(2 DOWNTO 0) & "00000000000000000000000000000";
ELSIF B(4 DOWNTO 0) = "11110"
   THEN
	 o:= a(1 DOWNTO 0) & "000000000000000000000000000000";
ELSIF B(4 DOWNTO 0) = "11111"
   THEN
	 o:= a(0) & "0000000000000000000000000000000";
else
    o:= a(31 DOWNTO 0);
end if;
    return  o;
  end function LS;
begin
      process(Register_S, Register_T, ALU_CONTROL)
		begin
			case ALU_CONTROL is
					when "0000" =>  
						Temp_Result <= Register_S + Register_T ;				
               when "0001" => 
						Temp_Result <= Register_S - Register_T;
               when "0010" => 
						Temp_Result <= Register_S and Register_T;
					when "0011" =>  
						Temp_Result <= Register_S or Register_T;
					when "0100" =>  
						Temp_Result <= Register_S nor Register_T;
					when "1010" => 
						Temp_Result <= Register_S - Register_T;
               when "0101" =>  
						Temp_Result <= Register_S - Register_T;
               when "0110" =>  
						Temp_Result <= Register_S - Register_T;
					when "0111" => 
					   Temp_Result <= LS(Register_S,Register_T);
					when others =>
			end case;
		end process;
		Result <= Temp_Result;
		Zero_Flag <= '1' when (Temp_Result=(Temp_Result'range=>'0')) else '0'; 
end Behavioral;