library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;

entity PC_FF is
PORT (
Input_Address: in STD_LOGIC_VECTOR(31 DOWNTO 0);
clk: in STD_LOGIC;
reset: in STD_LOGIC;
decrypt: in STD_LOGIC;
encrypt: in STD_LOGIC;
Output_ready: in STD_LOGIC;
Ouput_Adress: out STD_LOGIC_VECTOR(31 DOWNTO 0));
end PC_FF;

architecture Behavioral of PC_FF is
begin
PROCESS (reset,clk,decrypt,encrypt)  BEGIN
  IF (reset='0') 
     THEN 
	  Ouput_Adress <= x"00000002"; 
  ELSIF(decrypt = '1' and Output_ready = '0')
     then 
	  Ouput_Adress <= x"0000003C";
  ELSIF(encrypt = '1' and Output_ready = '0')
     then 
	  Ouput_Adress <= x"00000006";	  
  ELSIF rising_edge(clk)
     THEN 
	  Ouput_Adress <= Input_Address; 
  END IF;                                      
END PROCESS;                                   
end Behavioral;