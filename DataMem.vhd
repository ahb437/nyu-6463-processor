----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:12:03 11/19/2018 
-- Design Name: 
-- Module Name:    Data_Memory_F - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;
USE ieee.std_logic_arith.all;

entity Data_Mem is
PORT (
A: in STD_LOGIC_VECTOR(31 DOWNTO 0);
Data_To_Write: in STD_LOGIC_VECTOR(31 DOWNTO 0);
Write_Enable: in STD_LOGIC;
clk: in STD_LOGIC; 	   
Read_Data: out STD_LOGIC_VECTOR(31 DOWNTO 0)
);
end Data_Mem;

architecture Behavioral of Data_Mem is
TYPE ram IS ARRAY (0 TO 31) OF STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL dmem: 
ram:=ram'(
x"00000000",
x"00000000",
x"46F8E8C5", 
x"460C6085",
x"70F83B8A", 
x"284B8303",
x"513E1454", 
x"F621ED22",
x"3125065D", 
x"11A83A5D", 
x"D427686B", 
x"713AD82D",
x"4B792F99", 
x"2799A4DD", 
x"A7901C49", 
x"DEDE871A",
x"36C03196",
x"A7EFC249", 
x"61A78BB8", 
x"3B0A1D2B",
x"4DBFCA76", 
x"AE162167", 
x"30D76B0A", 
x"43192304",
x"F6CC1431", 
x"65046380",
x"00000000",
x"00000000",
x"00000000",
x"00000000",
x"00000000", -- 30 A 
x"00000000"  -- 35 Dec-Done
);
begin
process(clk, Write_Enable)
begin
if falling_edge(clk) then
	Read_Data<=dmem(conv_integer(unsigned(A(4 downto 0))));
	if(Write_Enable='1')then
	dmem(conv_integer(unsigned(A(4 downto 0))))<=Data_To_Write;
	end if;	
end if;
end process;
end Behavioral;

