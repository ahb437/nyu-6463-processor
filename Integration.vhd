library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;
entity Integration is
PORT (
clk: in STD_LOGIC;
clr: in STD_LOGIC;
decrypt: in STD_LOGIC;
encrypt: in STD_LOGIC;
Register_24BD : in  STD_LOGIC_VECTOR (31 downto 0);
PC : out STD_LOGIC_VECTOR(31 downto 0);
Reg1: out STD_LOGIC_VECTOR(31 DOWNTO 0);
output_rdy : out STD_LOGIC;
Register_24i : out  STD_LOGIC_VECTOR (31 downto 0);
Register_25i : out  STD_LOGIC_VECTOR (31 downto 0)
);

end Integration;

architecture Behavioral of Integration is

component Arithmetic_Logic_Unit is 
    Port ( Register_S : in  STD_LOGIC_VECTOR (31 downto 0);
           Register_T : in  STD_LOGIC_VECTOR (31 downto 0);
           ALU_CONTROL : in  STD_LOGIC_VECTOR (3 downto 0);
           Zero_Flag : out  STD_LOGIC;
           Result : out  STD_LOGIC_VECTOR (31 downto 0)
			  );
end component;

component Decoder is
    Port ( Opcode : in  STD_LOGIC_VECTOR (5 downto 0);
           function_code : in  STD_LOGIC_VECTOR (5 downto 0);
			  decrypt: in STD_LOGIC;
			  encrypt: in STD_LOGIC;
			  output_rdy : out STD_LOGIC;
			  Jump : out STD_LOGIC;
           Load_Memory_To_Reg : out  STD_LOGIC;
           Write_To_Memory : out  STD_LOGIC;
           Branch : out  STD_LOGIC;
           Signal_to_ALU  : out  STD_LOGIC_VECTOR (3 downto 0);
           Source : out  STD_LOGIC;
           Reg : out  STD_LOGIC;
           Write_To_Register : out  STD_LOGIC;
			  Halt : out STD_LOGIC
         );
end component;


component Data_Mem
PORT (
A: in STD_LOGIC_VECTOR(31 DOWNTO 0);
Data_To_Write: in STD_LOGIC_VECTOR(31 DOWNTO 0);
Write_Enable: in STD_LOGIC;
clk: in STD_LOGIC; 	   
Read_Data: out STD_LOGIC_VECTOR(31 DOWNTO 0)
);
end component;

component IM is
    Port ( Address : in  STD_LOGIC_VECTOR (31 downto 0);
           Data : out  STD_LOGIC_VECTOR (31 downto 0));
end component;

component PC_FF is
PORT (
Input_Address: in STD_LOGIC_VECTOR(31 DOWNTO 0);
clk: in STD_LOGIC;
reset: in STD_LOGIC;
decrypt: in STD_LOGIC;
encrypt: in STD_LOGIC;
Output_ready: in STD_LOGIC;
Ouput_Adress: out STD_LOGIC_VECTOR(31 DOWNTO 0));
end component;

component Reg is
    Port ( clk : in  STD_LOGIC;
	        clr : in  STD_LOGIC;
           Write_Enable : in  STD_LOGIC; 
           Register_S_Addr : in  STD_LOGIC_VECTOR (4 downto 0);
           Register_T_Addr : in  STD_LOGIC_VECTOR (4 downto 0);
			  Register_24BD : in  STD_LOGIC_VECTOR (31 downto 0);
           Register_To_Write_Addr : in  STD_LOGIC_VECTOR (4 downto 0);
           Data_To_Write : in  STD_LOGIC_VECTOR (31 downto 0);
			  Register_24 : out  STD_LOGIC_VECTOR (31 downto 0);
           Register_25 : out  STD_LOGIC_VECTOR (31 downto 0);
           Register_S : out  STD_LOGIC_VECTOR (31 downto 0);
           Register_T : out  STD_LOGIC_VECTOR (31 downto 0)
			  );
end component;

component Extender is
PORT (
		Immediate_Value: in STD_LOGIC_VECTOR(15 DOWNTO 0);
		Extended_Value: out STD_LOGIC_VECTOR(31 DOWNTO 0)
     );
end component;


signal Program_Counter_Value : STD_LOGIC_VECTOR(31 downto 0);
signal Instruction_Address : STD_LOGIC_VECTOR(31 downto 0);
signal Instruction : STD_LOGIC_VECTOR(31 downto 0);
signal Register_Destination : STD_LOGIC_VECTOR(4 downto 0);
signal Output : STD_LOGIC_VECTOR(31 downto 0);
signal Write_To_Register : STD_LOGIC;
signal R1 : STD_LOGIC_VECTOR(31 downto 0);
signal R2 : STD_LOGIC_VECTOR(31 downto 0);
signal Immediate_Value : STD_LOGIC_VECTOR(31 downto 0);
signal Input_B_Select : STD_LOGIC_VECTOR(31 downto 0);
signal ALUControl: STD_LOGIC_VECTOR(3 downto 0);
signal ZeroFlag : STD_LOGIC;
signal ALU_Output : STD_LOGIC_VECTOR(31 downto 0);
signal Write_To_Memory : STD_LOGIC;
signal Data_Memory_Output : STD_LOGIC_VECTOR(31 downto 0);
signal Jump_Flag : STD_LOGIC;
signal Memory_To_Register : STD_LOGIC;
signal ALU_Input_Select : STD_LOGIC;
signal RegDstSignal : STD_LOGIC;
signal Branch_Flag : STD_LOGIC;
signal PC_Input_Select : STD_LOGIC_VECTOR(1 downto 0):="00";
signal Normal_PC : STD_LOGIC_VECTOR(31 downto 0):=x"00000000";
signal Branch_PC : STD_LOGIC_VECTOR(31 downto 0):=x"00000000";
signal Jump_PC : STD_LOGIC_VECTOR(31 downto 0):=x"00000000";
signal Halt : STD_LOGIC := '1';
signal clk1 : STD_LOGIC ;
signal output_ready : STD_LOGIC ;

begin
clk1 <= clk and Halt;

Program_Counter: PC_FF PORT MAP(Program_Counter_Value,clk1,clr,decrypt,encrypt,output_ready,Instruction_Address);

Instruction_Set: IM PORT MAP(Instruction_Address,Instruction);

RegF: Reg PORT MAP(clk1,clr,Write_To_Register,Instruction(25 downto 21),Instruction(20 downto 16),Register_24BD,Register_Destination,Output,Register_24i,Register_25i,R1,R2);

Sign_Extender: Extender PORT MAP(Instruction(15 downto 0),Immediate_Value);

AL: Arithmetic_Logic_Unit PORT MAP(R1,Input_B_Select,ALUControl,ZeroFlag,ALU_Output);

DM: Data_Mem PORT MAP(ALU_Output,R2,Write_To_Memory,clk1,Data_Memory_Output);

CU: Decoder PORT MAP(Instruction(31 downto 26),Instruction(5 downto 0),decrypt,encrypt,output_ready,Jump_Flag,Memory_To_Register,Write_To_Memory,Branch_Flag,ALUControl,ALU_Input_Select,RegDstSignal,Write_To_Register,Halt);
output_rdy <= output_ready;
with RegDstSignal select
Register_Destination <= Instruction(15 downto 11) when '1',
					 Instruction(20 downto 16) when others;
					 
with ALU_Input_Select select
Input_B_Select <= Immediate_Value when '1',
					 R2 when others;

with Memory_To_Register select
Output <= Data_Memory_Output when '1',
						ALU_Output when others;

with PC_Input_Select select
Program_Counter_Value <= Branch_PC when "01",
				Jump_PC when "10",
				Normal_PC when others;

PC_Input_Select(1)<=Jump_Flag;
with Instruction(31 downto 26) select
PC_Input_Select(0)<= Branch_Flag and not(ZeroFlag) when "001011",
                     Branch_Flag and ZeroFlag when others;
					  
Normal_PC<=Instruction_Address + x"00000002";

Branch_PC<=Instruction_Address+Immediate_Value + x"00000002";

Jump_PC(31 downto 26)<=Normal_PC(31 downto 26);
Jump_PC(25 downto 0)<=Instruction(25 downto 0);
PC <= Program_Counter_Value;
Reg1<=Output;
end Behavioral;

