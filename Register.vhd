-- The register file should be able to write one and read data twice as per the task given. 
-- We have only used 16 registers for this 32 bit processor, since it wasnt required. If more are required we can simply 
-- increase the number of registers in the type definition and modify the 
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;
entity Reg is
    Port ( clk : in  STD_LOGIC;
	 	     clr : in  STD_LOGIC;
           Write_Enable : in  STD_LOGIC; 
           Register_S_Addr : in  STD_LOGIC_VECTOR (4 downto 0);
           Register_T_Addr : in  STD_LOGIC_VECTOR (4 downto 0);
			  Register_24BD : in  STD_LOGIC_VECTOR (31 downto 0);
           Register_To_Write_Addr : in  STD_LOGIC_VECTOR (4 downto 0);
           Data_To_Write : in  STD_LOGIC_VECTOR (31 downto 0); 
			  Register_24 : out  STD_LOGIC_VECTOR (31 downto 0);
			  Register_25 : out  STD_LOGIC_VECTOR (31 downto 0);
			  Register_S : out  STD_LOGIC_VECTOR (31 downto 0);
           Register_T : out  STD_LOGIC_VECTOR (31 downto 0) 
         );
end Reg;
architecture Behavioral of Reg is 

TYPE ram IS ARRAY (0 TO 31) OF STD_LOGIC_VECTOR(31 DOWNTO 0);

SIGNAL r:ram:=ram'(x"00000000", x"00000000", x"00000000", x"00000000",
						 x"00000000", x"00000000", x"00000000", x"00000000",
						 x"00000000", x"00000000", x"80000000", x"00000000",
						 x"00000000", x"00000000", x"00000000", x"00000000",
						 x"00000000", x"00000000", x"00000000", x"00000000",
						 x"00000000", x"00000000", x"00000000", x"00000000",
						 x"00000000", x"00000000", x"00000000", x"00000000", -- r24 r25
						 x"00000000", x"00000000", x"00000000", x"00000000"
						 );	
begin
 	process(clk,Write_Enable,clr) 
	begin
   if (clr = '0')
	then 
	r <= ram'(x"00000000", x"00000000", x"00000000", x"00000000",
						 x"00000000", x"00000000", x"00000000", x"00000000",
						 x"00000000", x"00000000", x"80000000", x"00000000",
						 x"00000000", x"00000000", x"00000000", x"00000000",
						 x"00000000", x"00000000", x"00000000", x"00000000",
						 x"00000000", x"00000000", x"00000000", x"00000000",
						 x"00000000", Register_24BD, x"00000000", x"00000000", -- r24 r25
						 x"00000000", x"00000000", x"00000000", x"00000000"
						 );	
   else		
		if rising_edge(clk) and Write_Enable = '1'
         then 
		   r((conv_integer(Register_To_Write_Addr(4 downto 0)))) <= Data_To_Write;
		   else
		end if;	

   end if;
	end process;
	Register_S <= r(conv_integer(Register_S_Addr(4 downto 0))); 
	Register_T <= r(conv_integer(Register_T_Addr(4 downto 0)));
	Register_24 <= r(24); 
	Register_25 <= r(25);
end Behavioral;

