--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   13:42:08 12/13/2018
-- Design Name:   
-- Module Name:   C:/Users/vm148/Desktop/Final - Copy (5)/Final/Final.vhd
-- Project Name:  Final
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: Integration
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
use std.env.all;
use	IEEE.STD_LOGIC_TEXTIO.ALL;
use	STD.TEXTIO.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY Final IS
END Final;
 
ARCHITECTURE behavior OF Final IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT Integration
    PORT(
         clk : IN  std_logic;
         clr : IN  std_logic;
         decrypt : IN  std_logic;
         encrypt : IN  std_logic;
         Register_24BD : IN  std_logic_vector(31 downto 0);
         PC : OUT  std_logic_vector(31 downto 0);
         Reg1 : OUT  std_logic_vector(31 downto 0);
         output_rdy : OUT  std_logic;
         Register_24i : OUT  std_logic_vector(31 downto 0);
         Register_25i : OUT  std_logic_vector(31 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal clr : std_logic := '0';
   signal decrypt : std_logic := '0';
   signal encrypt : std_logic := '0';
   signal Register_24BD : std_logic_vector(31 downto 0) := (others => '0');

 	--Outputs
   signal PC : std_logic_vector(31 downto 0);
   signal Reg1 : std_logic_vector(31 downto 0);
   signal output_rdy : std_logic;
   signal Register_24i : std_logic_vector(31 downto 0);
   signal Register_25i : std_logic_vector(31 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: Integration PORT MAP (
          clk => clk,
          clr => clr,
          decrypt => decrypt,
          encrypt => encrypt,
          Register_24BD => Register_24BD,
          PC => PC,
          Reg1 => Reg1,
          output_rdy => output_rdy,
          Register_24i => Register_24i,
          Register_25i => Register_25i
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
  Test_process: process
		VARIABLE in_line : LINE;
      VARIABLE input : std_logic_vector(31 downto 0);   	
      file outfile : text;	
		VARIABLE out_line1 : LINE;
      VARIABLE output1 : std_logic_vector(63 downto 0);   	
      file infile : text;			
		variable Input1: integer := 0;
   begin
     -- This portion is to test encoding of an input then decoding of the input. 	  
	  file_open(infile,"inputs.txt", read_mode);
		file_open(outfile,"outputs.txt", read_mode);
		while not (endfile(infile)) loop
		READLINE( infile, in_line);  
	   HREAD( in_line, input);
		Input1 := Input1 + 1;
      clr <= '0';
      Register_24BD <= input;
      wait for 100 ns;		
	   clr <= '1';
		decrypt <= '0';
		encrypt <= '0';
		wait for 300 ns;
		encrypt <= '1';
		wait for 30 ns;
		encrypt <= '0';
		wait until output_rdy = '1';
		READLINE( outfile, out_line1);  
	   HREAD( out_line1, output1);
		assert (output1 = (Register_24i & Register_25i)) report "Check failed!" severity failure;
			report "Encoding was completed succesfully for the current plain text input, Input Number" & integer'image(Input1);
		wait for 300 ns;
		decrypt <= '1';
		wait for 30 ns;
		decrypt <= '0';
		wait until output_rdy = '1';
		assert (input = (Register_25i & Register_24i)) report "Check failed!" severity failure;
		report "Decoding was completed succesfully for the current input which was first encrypted, Input Number" & integer'image(Input1);
      wait for 300 ns;
		end loop;
	   file_close(infile);
		file_close(outfile);
		
     -- This portion is to test encoding of an input then encoding the encoded value again of the input. 		
	  file_open(infile,"inputs.txt", read_mode);
		file_open(outfile,"outputs1.txt", read_mode);
		while not (endfile(infile)) loop
		READLINE( infile, in_line);  
	   HREAD( in_line, input);
		Input1 := Input1 + 1;
      clr <= '0';
      Register_24BD <= input;
      wait for 100 ns;		
	   clr <= '1';
		decrypt <= '0';
		encrypt <= '0';
		wait for 300 ns;
		encrypt <= '1';
		wait for 30 ns;
		encrypt <= '0';
		wait until output_rdy = '1';
		wait for 300 ns;
		Encrypt <= '1';
		wait for 30 ns;
		Encrypt <= '0';
		wait until output_rdy = '1';
		READLINE( outfile, out_line1);  
	   HREAD( out_line1, output1);
		assert (output1 = (Register_24i & Register_25i)) report "Check failed!" severity failure;
			report "Encoding was completed succesfully for the current plain text input, Input Number" & integer'image(Input1);
      wait for 300 ns;
		end loop;
	   file_close(infile);
		file_close(outfile);		
		
		
	   -- This portion is to test decoding of an input that was an encrypted text value again of the input. 		
	  file_open(infile,"inputs2.txt", read_mode);
		file_open(outfile,"outputs2.txt", read_mode);
		while not (endfile(infile)) loop
		READLINE( infile, in_line);  
	   HREAD( in_line, input);
		Input1 := Input1 + 1;
      clr <= '0';
      Register_24BD <= input;
      wait for 100 ns;		
	   clr <= '1';
		decrypt <= '1';
		encrypt <= '0';
		wait for 300 ns;
		decrypt <= '1';
		wait for 30 ns;
		decrypt <= '0';
		wait until output_rdy = '1';
		READLINE( outfile, out_line1);  
	   HREAD( out_line1, output1);
		assert (output1 = (Register_24i & Register_25i)) report "Check failed!" severity failure;
			report "Decoding was completed succesfully for the current encrypted text input, Input Number" & integer'image(Input1);
		wait for 300 ns;
		end loop;
	   file_close(infile);
		file_close(outfile);		
		finish(0);
   end process;

END;
