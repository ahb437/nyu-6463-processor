library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
entity Extender is
PORT (
		Immediate_Value: in STD_LOGIC_VECTOR(15 DOWNTO 0);
		Extended_Value: out STD_LOGIC_VECTOR(31 DOWNTO 0)
		);
end Extender;

architecture Behavioral of Extender is
begin
Extended_Value(15 downto 0)<=Immediate_Value; 
with Immediate_Value(15) select		    
Extended_Value(31 downto 16) <= "1111111111111111"  when '1',
											"0000000000000000" when others;	
end Behavioral;
